const configuration = {
	ApiUrl: 'http://localhost:5000',
	token: null,
}

export default configuration;

export const setApiRequestToken = (token) => {
  Object.assign(configuration, {token: token});
};