import { configureStore } from '@reduxjs/toolkit';
import storage from "redux-persist/lib/storage";
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import thunk from "redux-thunk";

import userReducer from './userReducer';
import productReducer from './productReducer';
import appReducer from './appReducer';
import authReducer from './authReducer';
import tokenReducer from './tokenReducer';

const persistConfig = {
	key: "root",
	storage,
  whitelist: ['auth'],
  version: 2,
  };

  const reducers = combineReducers({
	app: appReducer,
	user: userReducer,
	product: productReducer,
	auth: authReducer,
	token: tokenReducer
  });

  const persistedReducer = persistReducer(persistConfig, reducers);

  export const store = configureStore({
	reducer: persistedReducer,
	devTools: process.env.NODE_ENV !== "production",
	middleware: [thunk],
  });
