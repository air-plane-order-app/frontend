import { createSlice } from '@reduxjs/toolkit';
import { setApiRequestToken } from '../configuration';

const initialState = {
  user: [],
  isLogged: false,
  isAdmin: false, 
  isSupport: false,
};

export const counterSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    isLoggedIn: (state, action) => {
      state.isLogged = action.payload.result;
      setApiRequestToken(action.payload.token);
    },
    updateUser: (state, action) => {
      return {...state, user: action.payload.user, isAdmin: action.payload.isAdmin}
    },
    updateSupport: (state, action) => {
      console.log(action.payload, state.isSupport)
      return {...state, isSupport: action.payload}
    },
  },
});

export const { isLoggedIn, updateUser, updateSupport} = counterSlice.actions;

export default counterSlice.reducer;
