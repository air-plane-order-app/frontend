import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  token: null
};

export const counterSlice = createSlice({
  name: 'token',
  initialState,
  reducers: {
    updateToken: (state, action) => {
      state.token = action.payload;
    },
  },
});

export const { updateToken } = counterSlice.actions;

export default counterSlice.reducer;
