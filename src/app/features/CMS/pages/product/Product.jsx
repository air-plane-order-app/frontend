import { Link } from 'react-router-dom';
import './product.css';
import Chart from '../../components/chart/Chart';
import { productData } from '../../../../dummyData';
import { Publish } from '@material-ui/icons';

export default function Product() {
  return (
    <div className="product">
      <div className="productTitleContainer">
        <h1 className="productTitle">Product</h1>
      </div>
      <div className="productBottom">
        <form className="productForm">
          <div className="productFormLeft">
            <input type="text" placeholder="From" />
            <input type="text" placeholder="To" />
            <input type="text" placeholder="Departure Start Time" />
            <input type="text" placeholder="Departure End Time" />
            <select name="type" id="type">
              <option value="roundtrip">Roundtrip</option>
              <option value="oneway">One-way</option>
            </select>
          </div>
          <div className="productFormRight">
            <input type="text" placeholder="Arrival Start Time" />
            <input type="text" placeholder="Arrival End Time" />
            <input type="text" placeholder="Eco" />
            <input type="text" placeholder="Business" />
            <input type="text" placeholder="First" />
            <input type="text" placeholder="Available" />
          </div>
          <div>
            <button>Update</button>
          </div>
        </form>
      </div>
    </div>
  );
}
