import { useState } from 'react';
import { register } from '../../../../core/apis';
import './newUser.css';

export default function NewUser() {
  const [data, setData] = useState({
    name: '',
    email: '',
    password: '',
    isVerified: false,
    role: {
      roleName: 'admin',
    },
  });

  const onChange = (e, type) => {
    const target = e.target;
    const value = target.value;
    console.log(value);
    let dataChange = { ...data };

    if (type === 'role') {
      let role;
      if (value) {
        role = {
          roleName: 'admin',
        };
      } else {
        role = {
          roleName: 'user',
        };
      }

      dataChange[type] = role;
    } else {
      dataChange[type] = value;
    }

    setData(dataChange);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    console.log('ok');
    const res = await register(data);

    if (!!res.data) {
      window.location.href = '/cms/users';
    }
  };

  return (
    <div className="newUser">
      <h1 className="newUserTitle">New User</h1>
      <form className="newUserForm" onSubmit={onSubmit}>
        <div className="newUserItem">
          <label>Full Name</label>
          <input
            type="text"
            placeholder="John Smith"
            onChange={(e) => onChange(e, 'name')}
          />
        </div>
        <div className="newUserItem">
          <label>Email</label>
          <input
            type="email"
            placeholder="john@gmail.com"
            onChange={(e) => onChange(e, 'email')}
          />
        </div>
        <div className="newUserItem">
          <label>Password</label>
          <input
            type="password"
            placeholder="password"
            onChange={(e) => onChange(e, 'password')}
          />
        </div>
        <div className="newUserItem">
          <label>Phone</label>
          <input type="text" placeholder="+1 123 456 78" />
        </div>
        <div className="newUserItem">
          <label>Address</label>
          <input type="text" placeholder="New York | USA" />
        </div>
        <div className="newUserItem">
          <label>Gender</label>
          <div className="newUserGender">
            <input type="radio" name="gender" id="male" value="male" />
            <label for="male">Male</label>
            <input type="radio" name="gender" id="female" value="female" />
            <label for="female">Female</label>
            <input type="radio" name="gender" id="other" value="other" />
            <label for="other">Other</label>
          </div>
        </div>
        <div className="newUserItem">
          <label>Admin</label>
          <select
            className="newUserSelect"
            name="active"
            id="active"
            onChange={(e) => onChange(e, 'role')}
          >
            <option value={true}>Yes</option>
            <option value={false}>No</option>
          </select>
        </div>
        <button className="newUserButton">Create</button>
      </form>

    </div>
  );
}
