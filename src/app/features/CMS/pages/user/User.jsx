import React, { useState, useEffect} from 'react';
import {
  CalendarToday,
  LocationSearching,
  MailOutline,
  PermIdentity,
  PhoneAndroid,
  Publish,
} from '@material-ui/icons';
import { Link, useHistory} from 'react-router-dom';
import './user.css';
import { updateInfo } from '../../../../core/apis';
import { useLocation } from 'react-router-dom';
import { useTheme } from '@material-ui/styles';
import { getUserById } from '../../../../core/apis';
import { updateUser } from '../../../../../store/authReducer';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';

const initialState = {
  name: '',
  phone: '',
  address: '',
  idNumber: '',
  birthDate: '',
};
export default function User() {
  const [firstInfo, setFirstInfo] = useState(initialState);
  const [info, setInfo] = useState(initialState);
  const [url, setUrl] = useState(null);
  const user = useSelector((state) => state.auth.user);

  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setInfo({ ...info, [name]: value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const idUser = location.pathname.split('/')[location.pathname.split('/')?.length - 1]
      const res = await updateInfo( idUser, info );
      console.log(res?.data)
    } catch (err) {
      err?.response?.data?.msg &&
        setInfo({ ...info});
    }
    history.replace('/cms/users')
  };

  useEffect( async () => {
    const idUser = location.pathname.split('/')[location.pathname.split('/')?.length - 1]
    const result = await getUserById(idUser);
    console.log(result.data)
    if (user._id === result.data?.user?._id) {
      dispatch(updateUser({user: result.data?.user, isAdmin: result.data?.user?.role?.roleName === 'admin'})); 
    }

    setFirstInfo(result.data)
  }, [location]);

  const updateFile = async (e) => {
    const src = URL.createObjectURL(e.target.files[0])
    const base64 = await getBase64ImageFromUrl(src);
    setInfo({
      ...info,
      avatar: base64,
    })
    console.log(base64)
    setUrl(base64)
  }

  const getBase64ImageFromUrl = async (imageUrl) => {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }

  return (
    <div className="user">
      <div className="userTitleContainer">
        <h1 className="userTitle">Edit User</h1>
        <Link to="/cms/newUser">
          <button className="userAddButton">Create</button>
        </Link>
      </div>
      <div className="userContainer">
        <div className="userShow">
          <div className="userShowTop">
            <img
              src={url ?? firstInfo.avatar}
              alt=""
              className="userShowImg"
            />
            <div className="userShowTopTitle">
              <span className="userShowUsername">{firstInfo.name}</span>
              <span className="userShowUserTitle">{firstInfo.idNumber}</span>
            </div>
          </div>
          <div className="userShowBottom">
            <span className="userShowTitle">Account Details</span>
            <div className="userShowInfo">
              <PermIdentity className="userShowIcon" />
              <span className="userShowInfoTitle">{firstInfo.birthDate}</span>
            </div>
            <div className="userShowInfo">
              <PhoneAndroid className="userShowIcon" />
              <span className="userShowInfoTitle">{firstInfo.phone}</span>
            </div>
            <div className="userShowInfo">
              <LocationSearching className="userShowIcon" />
              <span className="userShowInfoTitle">{firstInfo.address}</span>
            </div>
          </div>
        </div>
        <div className="userUpdate">
          <span className="userUpdateTitle">Edit</span>
          <form className="userUpdateForm" onSubmit={handleSubmit}>
            <div className="userUpdateLeft">
              <div className="userUpdateItem">
                <label>Full Name</label>
                <input
                  type="text"
                  placeholder="Enter Your Name"
                  className="userUpdateInput"
                  name="name"
                  onChange={handleChangeInput}
                />
              </div>
              <div className="userUpdateItem">
                <label>Phone</label>
                <input
                  type="text"
                  placeholder="Enter Your Phone"
                  className="userUpdateInput"
                  name="phone"
                  onChange={handleChangeInput}
                />
              </div>
              <div className="userUpdateItem">
                <label>Address</label>
                <input
                  type="text"
                  placeholder="Enter Your Address"
                  className="userUpdateInput"
                  name="address"
                  onChange={handleChangeInput}
                />
              </div>
              <div className="userUpdateItem">
                <label>ID Number</label>
                <input
                  type="text"
                  placeholder="Enter Your ID Number"
                  className="userUpdateInput"
                  name="idNumber"
                  onChange={handleChangeInput}
                />
              </div>
              <div className="userUpdateItem">
                <label>Birth Date</label>
                <input
                  type="text"
                  placeholder="Enter Your Birth Date"
                  className="userUpdateInput"
                  name="birthDate"
                  onChange={handleChangeInput}
                />
              </div>
            </div>
            <div className="userUpdateRight">
              <div className="userUpdateUpload">
                <img
                  className="userUpdateImg"
                  src={url ?? firstInfo.avatar}
                  alt=""
                />
                <label htmlFor="file">
                  <Publish className="userUpdateIcon" />
                </label>
                <input onChange={updateFile} type="file" id="file" style={{ display: 'none' }} />
              </div>
              <button className="userUpdateButton" type = "submit">Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
