import './userList.css';
import { DataGrid } from '@material-ui/data-grid';
import { DeleteOutline } from '@material-ui/icons';
import { userRows } from '../../../../dummyData';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { getAllUsers, deleteInfo } from '../../../../core/apis';
import { useHistory } from 'react-router-dom';


export default function UserList() {

  const [data, setData] = useState([]);
  const history = useHistory();
  useEffect(() => {
    (async () => {
      const res = await getAllUsers();
      // console.log(res?.data);
      setData(
        res?.data?.map((item) => {
          item.id = item._id;
          return item;
        }),
      );
    })();
  }, []);

  const handleDelete = async (id) => {
    const res = await deleteInfo(id);
    // console.log(res?.data)
    if (res?.data) {
      window.location.reload();
    }
  };
  const handleEdit = async (id) => {
    history.replace({
      pathname: `/cms/user/${id}`,
    });
  };
  const columns = [
    { field: 'id', headerName: 'ID', width: 250 },
    {
      field: 'name',
      headerName: 'User',
      width: 200,
      renderCell: (params) => {
        return (
          <div className="userListUser">
            <img className="userListImg" src={params.row?.avatar} alt="" />
            {params.row.name}
          </div>
        );
      },
    },
    { field: 'email', headerName: 'Email', width: 200 },
    {
      field: 'isVerified',
      headerName: 'Verified',
      width: 120,
    },
    {
      field: 'action',
      headerName: 'Action',
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <Link to={'/cms/user/' + params.row.id}>
              <button
                className="userListEdit"
                onClick={() => handleEdit(params.row.id)}
              >
                Edit
              </button>
            </Link>
            <DeleteOutline
              className="userListDelete"
              onClick={() => handleDelete(params.row.id)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div className="userList">
      <div className="userAddButtonContainer">
        <Link to="/cms/newuser">
          <button className="userAddButton">Create</button>
        </Link>
      </div>
      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
        checkboxSelection
      />
    </div>
  );
}
