import "./productList.css";
import { DataGrid } from "@material-ui/data-grid";
import { DeleteOutline } from "@material-ui/icons";
import { productRows } from "../../../../dummyData";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import { deleteFlight, getAllFlightsNoPaged } from "../../../../core/apis";

import useState from 'react-usestateref'

export default function ProductList() {
  const [data, setData] = useState([]);

  useEffect(() => {
    (async () => {
      const res = await getAllFlightsNoPaged();
      setData(res?.data?.flight.map(item => {
        item.id = item._id
        return item;
      }))
    })()
  },[])

  const handleDelete = async (id) => {
    const res = await deleteFlight({id});
    console.log(res?.data)
    if (res?.data?.flight) {
      window.location.reload()
    }
  };

  const columns = [
    { field: "id", headerName: "ID", width: 250 },
    {
      field: "departureFromLocation",
      headerName: "From",
      width: 150,
      renderCell: (params) => {
        return (
          <div className="productListItem">
            {params.row.departureFromLocation}
          </div>
        );
      },
    },
    {
      field: "departureToLocation",
      headerName: "To",
      width: 150,
      renderCell: (params) => {
        return (
          <div className="productListItem">
            {params.row.departureToLocation}
          </div>
        );
      },
    },
    { field: "type", headerName: "Type", width: 200 },
    { field: "flightStartTime", headerName: "Departure Start Time", width: 200 },
    { field: "flightEndTime", headerName: "Departure End Time", width: 200 },
    { field: "flightArrivalStartTime", headerName: "Arrival Start Time", width: 200 },
    { field: "flightArrivalEndTime", headerName: "Arrival End Time", width: 200 },
    {
      field: "ecoClass",
      headerName: "Eco",
      width: 160,
    },
    {
      field: "businessClass",
      headerName: "Business",
      width: 160,
    },
    {
      field: "firstClass",
      headerName: "First",
      width: 160,
    },
    {
      field: "availableSeat",
      headerName: "Available",
      width: 160,
    },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <Link to={"/cms/product/" + params.row.id}>
              <button className="productListEdit">Edit</button>
            </Link>
            <DeleteOutline
              className="productListDelete"
              onClick={() => handleDelete(params.row.id)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div className="productList">
      <div className="productAddButtonContainer">
        <Link to="/cms/newproduct">
            <button className="productAddButton">Create</button>
        </Link>
      </div>
      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
        checkboxSelection
      />
    </div>
  );
}
