import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { DataGrid } from '@material-ui/data-grid';
import { DeleteOutline } from '@material-ui/icons';
import { productRows } from '../../../../dummyData';
import { Link } from 'react-router-dom';
import '../productList/productList.css';
import { getMessage } from '../../../../core/apis';
import moment from 'moment';

const MessageList = () => {
  const [data, setData] = useState([]);

  const columns = [
		{ field: 'id', headerName: 'ID', width: 250 },
    { field: 'room', headerName: 'Room ID', width: 250 },
    {
      field: 'name',
      headerName: 'User',
      width: 300,
    },
		{
      field: 'createAt',
      headerName: 'Create At',
      width: 300,
			renderCell: (params) => {
        return (
          <>{moment(new Date(params.row.createdAt)).format('DD-MM-YYYY HH:mm:ss').toString()}</>
        );
      },
    },
		{
      field: "action",
      headerName: "Action",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <Link to={"/cms/message/" + params.row.room}>
              <button className="productListEdit">Join Room</button>
            </Link>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    (async () => {
      const res = await getMessage();

      if (res.data?.message) {
				const data = res.data.message?.map(i => {
					return {
						...i,
						id: i._id,
					}
				})
        setData(data);
      }
    })();
  }, []);

  return (
    <div className="productList">
      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
        checkboxSelection
      />
    </div>
  );
};

export default MessageList;
