import React, { useEffect, useState, useContext } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './Message.css';
import { updateSupport } from '../../../../../store/authReducer';
import { useLocation } from 'react-router-dom';
import { SocketContext } from '../../../../context/socket/Socket';
import { deleteMessage } from '../../../../core/apis';

const Message = () => {
  const socket = useContext(SocketContext);
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const location = useLocation();

  const { isAdmin, user} = auth;
  const room = location.pathname.split('/')[location.pathname.split('/')?.length - 1]

  const [data, setData] = useState({
    messages: [
      {
        message:
          'Welcome! Type a message and press Send Message to continue the chat. Your customer is waiting for you',
        author: 'Bot',
      },
    ],
    input: '',
  });

  useEffect(() => {
    socket.emit('joinRoom', { name: `Admin (${user.name})`, room: room });

    dispatch(updateSupport(false));

    return () => {
      handleEndRoom();
      socket.disconnect();
    };
  }, []);

  useEffect(() => {
    socket.on('message', ({ author, message }) => {
      console.log(`${author}: `, message);

      let messages = [...data.messages];

      messages.push({ author, message });
      setData({
        ...data,
        messages: messages,
      });

      console.log('messages',messages)
    });
  });

  const updateInput = (e) => {
    setData({
      ...data,
      input: e.target.value,
    });
  };

  const handleMessage = () => {
    const author = isAdmin ? 'Admin' : user?.name;

    if (data.input !== '') {
      console.log(socket)
      socket.emit('message', {
        message: data.input,
        author: author,
        room: room,
      });
      setData({
        ...data,
        input: '',
      });
    }
  };

  const handleEndRoom = async () => {
    const res = await deleteMessage({room: room});

    if (!!res?.data) {
      window.location.href = '/cms/messageList'
    }
  }

  let msgIndex = 0;
  return (
    <div className="Message">
      <div className="Message-chatbox">
        {data.messages.map((msg) => {
          msgIndex++;
          return (
            <div key={msgIndex}>
              <p>{msg.author}</p>
              <p>{msg.message}</p>
            </div>
          );
        })}
      </div>
      <input
        className="Message-Textarea"
        placeholder="Type your messsage here..."
        onChange={updateInput}
        value={data.input}
      />
      <div className="Message-Container">
        <p>
          <button onClick={handleMessage}>Send Message</button>
        </p>
        <p>
          <button className="End-Button" onClick={handleEndRoom}>End Room</button>
        </p>
      </div>
    </div>
  );
};

export default Message;
