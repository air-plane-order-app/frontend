import './newProduct.css';
import React, { useEffect, useState } from 'react';
import { Dropdown, Form, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import airports from '../../../../../airport-data.json';
import moment from 'moment';
import { createFlight } from '../../../../core/apis';

const TYPE = [
  {
    id: 'roundtrip',
    name: 'Round Trip',
  },
  {
    id: 'oneway',
    name: 'One Way',
  },
];

export default function NewProduct() {
  const [data, setData] = useState({
    airplaneName: '',
    departureFromLocation: '',
    departureToLocation: '',
    departureStartTime: '',
    departureEndTime: '',
    flightStartTime: '',
    flightEndTime: '',
    arrivalFromLocation: '',
    arrivalToLocation: '',
    arrivalStartTime: '',
    arrivalEndTime: '',
    flightArrivalStartTime: '',
    flightArrivalEndTime: '',
    availableSeat: 0,
    totalSeat: 0,
    ecoClass: 0,
    businessClass: 0,
    firstClass: 0,
    type: 'roundtrip',
  });

  const onHandleChange = (e, type) => {
    const target = e.target;
    const value = target.value;
    console.log(value);
    let dataChange = { ...data };

    if (
      type === 'departureStartTime' ||
      type === 'departureEndTime' ||
      type === 'arrivalStartTime' ||
      type === 'arrivalEndTime'
    ) {
      dataChange[type] = moment(new Date(value)).toISOString();
    } else if (
      type === 'ecoClass' ||
      type === 'businessClass' ||
      type === 'firstClass' ||
      type === 'totalSeat'
    ) {
      dataChange[type] = parseInt(value);
      if (type === 'totalSeat') {
        dataChange['availableSeat'] = parseInt(value);
      }
    } else {
      dataChange[type] = value;
    }

    setData(dataChange);
  };

  const onCreateProduct = async (event) => {
    event.preventDefault();
    const res = await createFlight(data);
    console.log(res?.data);
    if (!!res?.data?.flight) {
      window.location.href = '/cms/products';
    }
  };

  return (
    <div className="newProduct">
      <h1 className="addProductTitle">New Product</h1>
      <form className="addProductForm" onSubmit={onCreateProduct}>
        <div className="productContainer">
          <div className="addProductItem"></div>
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              {TYPE.find(i => i.id === data.type)?.name}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              {TYPE.map((c) => (
                <Dropdown.Item
                  value={c.id}
                  onClick={(e) => {
                    const data = {
                      target: {
                        value: c.id,
                      },
                    };
                    onHandleChange(data, 'type');
                  }}
                >
                  {c.name}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>

          <div className="textContainer">
            <div className="addProductItem">
              <label>Departure From Location</label>
              <select
                class="mdb-select md-form form-control"
                onChange={(e) => onHandleChange(e, 'departureFromLocation')}
                searchable="Search here.."
              >
                <option key="flying-from" value="" disabled selected>
                  Choose your country
                </option>
                {airports.map((item) => {
                  return (
                    <option key={item.Code} value={item.Code}>
                      {item.Code} - {item.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="addProductItem">
              <label>Departure To Location</label>
              <select
                class="mdb-select md-form form-control"
                onChange={(e) => onHandleChange(e, 'departureToLocation')}
                searchable="Search here.."
              >
                <option key="flying-from" value="" disabled selected>
                  Choose your country
                </option>
                {airports.map((item) => {
                  return (
                    <option key={item.Code} value={item.Code}>
                      {item.Code} - {item.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="addProductItem">
              <label>Departure Start Date</label>
              <input
                onChange={(e) => onHandleChange(e, 'departureStartTime')}
                class="form-control"
                type="date"
                required
              />
            </div>
            <div className="addProductItem">
              <label>Departure End Date</label>
              <input
                onChange={(e) => onHandleChange(e, 'departureEndTime')}
                class="form-control"
                type="date"
                required
              />
            </div>
            <div className="addProductItem">
              <label>Departure Start Time</label>
              <input
                onChange={(e) => onHandleChange(e, 'flightStartTime')}
                class="form-control"
                type="time"
                required
              />
            </div>
            <div className="addProductItem">
              <label>Departure End Time</label>
              <input
                onChange={(e) => onHandleChange(e, 'flightEndTime')}
                class="form-control"
                type="time"
                required
              />
            </div>
          </div>

          {data.type === 'roundtrip' && (
            <div>
              <div className="textContainer">
                <div className="addProductItem">
                  <label>Arrival From Location</label>
                  <select
                    class="mdb-select md-form form-control"
                    onChange={(e) => onHandleChange(e, 'arrivalFromLocation')}
                    searchable="Search here.."
                  >
                    <option key="flying-from" value="" disabled selected>
                      Choose your country
                    </option>
                    {airports.map((item) => {
                      return (
                        <option key={item.Code} value={item.Code}>
                          {item.Code} - {item.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="addProductItem">
                  <label>Arrival To Location</label>
                  <select
                    class="mdb-select md-form form-control"
                    onChange={(e) => onHandleChange(e, 'arrivalToLocation')}
                    searchable="Search here.."
                  >
                    <option key="flying-from" value="" disabled selected>
                      Choose your country
                    </option>
                    {airports.map((item) => {
                      return (
                        <option key={item.Code} value={item.Code}>
                          {item.Code} - {item.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <div className="addProductItem">
                  <label>Arrival Start Date</label>
                  <input
                    onChange={(e) => onHandleChange(e, 'arrivalStartTime')}
                    class="form-control"
                    type="date"
                    required
                  />
                </div>
                <div className="addProductItem">
                  <label>Arrival End Date</label>
                  <input
                    onChange={(e) => onHandleChange(e, 'arrivalEndTime')}
                    class="form-control"
                    type="date"
                    required
                  />
                </div>
                <div className="addProductItem">
                  <label>Arrival Start Time</label>
                  <input
                    onChange={(e) =>
                      onHandleChange(e, 'flightArrivalStartTime')
                    }
                    class="form-control"
                    type="time"
                    required
                  />
                </div>
                <div className="addProductItem">
                  <label>Arrival End Time</label>
                  <input
                    onChange={(e) => onHandleChange(e, 'flightArrivalEndTime')}
                    class="form-control"
                    type="time"
                    required
                  />
                </div>
              </div>
            </div>
          )}

          <div className="textContainer">
            <div className="addProductItem">
              <label>Total Seat</label>
              <input
                type="number"
                placeholder="999"
                value={data.totalSeat}
                onChange={(e) => onHandleChange(e, 'totalSeat')}
              />
            </div>
            <div className="addProductItem">
              <label>Eco</label>
              <input
                type="number"
                placeholder="999"
                value={data.ecoClass}
                onChange={(e) => onHandleChange(e, 'ecoClass')}
              />
            </div>
          </div>

          <div className="textContainer">
            <div className="addProductItem">
              <label>Business</label>
              <input
                type="number"
                placeholder="999"
                value={data.businessClass}
                onChange={(e) => onHandleChange(e, 'businessClass')}
              />
            </div>
            <div className="addProductItem">
              <label>First</label>
              <input
                type="number"
                placeholder="999"
                value={data.firstClass}
                onChange={(e) => onHandleChange(e, 'firstClass')}
              />
            </div>
          </div>
        </div>
        <div className="textContainer">
          <button className="addProductButton">Create</button>
        </div>
      </form>
    </div>
  );
}
