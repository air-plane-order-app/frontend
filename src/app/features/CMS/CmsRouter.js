import React from 'react';
import {Switch, Route} from 'react-router-dom'
import {useSelector} from 'react-redux'

//Pages
import UserList from "../../features/CMS/pages/userList/UserList";
import User from "../../features/CMS/pages/user/User";
import Home from "../../features/CMS/pages/home/Home";
import NewUser from "../../features/CMS/pages/newUser/NewUser";
import ProductList from "../../features/CMS/pages/productList/ProductList";
import Product from "../../features/CMS/pages/product/Product";
import Sidebar from "../../features/CMS/components/sidebar/Sidebar";
import Topbar from "../../features/CMS/components/topbar/Topbar";
import NewProduct from "../../features/CMS/pages/newProduct/NewProduct";
import Message from "../../features/CMS/pages/message/Message";
import MessageList from "../../features/CMS/pages/message/MessageList";

import "./cms.css";

export const CmsRouter = () => {
	const auth = useSelector(state => state.auth)
  const {isLogged, isAdmin} = auth

  return (
		true &&
			<section>
				<div className="containers">
					<Sidebar />
					<Switch>
						<Route exact path="/cms">
							<UserList />
						</Route>
						<Route path="/cms/users">
							<UserList />
						</Route>
						<Route path="/cms/user/:userId">
							<User/>
						</Route>
						<Route path="/cms/newUser">
							<NewUser />
						</Route>
						<Route path="/cms/products">
							<ProductList />
						</Route>
						<Route path="/cms/product/:productId">
							<Product />
						</Route>
						<Route path="/cms/newproduct">
							<NewProduct />
						</Route>
						<Route path="/cms/message/:room">
							<Message />
						</Route>
						<Route path="/cms/messageList">
							<MessageList />
						</Route>
					</Switch>
				</div>
			</section>
  );
};

export default CmsRouter;
