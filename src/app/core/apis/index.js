import axios from "axios";
import configuration from "../../../configuration";
import jwt from 'jwt-decode'
import moment from 'moment';
import Cookies from 'universal-cookie';
import { setApiRequestToken } from '../../../configuration';

const { ApiUrl } = configuration;


const instance = axios.create({
  baseURL: ApiUrl,
  timeout: 30 * 1000,
  headers: {
    "Content-Type": "application/json",
  },
});

instance.interceptors.request.use(
  function (config) {
    // console.log("API::REQUEST", config);
    const { token } = configuration;

    // console.log("token", token);
    if (token) {
      config.headers.Authorization = `Bearer ${token?.access?.token}`;
      return config;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

instance.interceptors.response.use((response) => {
  return response
}, error => {
  console.warn('Error status', error)

  const code = error.response.status
  const config = error.response.config
  const cookies = new Cookies();

  if (code === 401 && (cookies.get("isLoggedIn") === 'true')) {
    (async () => {
      const { token } = configuration;
      const data = await instance.post(`/api/user/refresh-token`,{refreshToken: token?.refresh?.token})

      if (data?.data) {
        if (data?.data?.msg === 'Please login now!') {
          window.location.href = '/login'
        }

        setApiRequestToken(data?.data?.tokens)
        cookies.set('token', data?.data?.tokens, { path: '/' });

        config.headers = {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${data?.data?.tokens?.access?.token}`
        };
        config.baseURL = ApiUrl
        config.timeout = 30 * 1000

        return instance(config)
      }
    })()
  }
})

export const _post = (action, data) => {
  return instance.post(action, data);
};

export const _get = (action, params) => {
  return instance.get(`${action}`, { params });
};

export const _put = (action, params) => {
  return instance.put(`${action}`, params);
};

export const _patch = (action, params) => {
  return instance.patch(`${action}`, { params } );
};

export const _delete = (action, params) => {
  return instance.delete(`${action}`, { params });
};

const getToken = (refreshToken) => {
	return _post(`/api/user/refresh-token`, refreshToken);
}
const getUser = () => {
	return _get(`/api/user/info`);
}
const getAllUsers = () => {
	return _get(`/api/user/all-info`);
}
const getUserByGmail = (gmail) => {
	return _get(`/api/user/info?email=${gmail}`);
}
const getUserById = (id) => {
	return _get(`/api/user/info/${id}`);
}
const login = (data) => {
  return _post(`/api/user/login`, data);
}
const googleLogin = (data) => {
  return _post(`/api/user/google-login`, data);
}
const facebookLogin = (data) => {
  return _post(`/api/user/facebook-login`, data);
}
const register = (data) => {
  return _post(`/api/user/register`, data)
}
const activeEmail = (data) => {
  return _post(`/api/user/activation`, data)
}
const forgotPassword = (data) => {
  return _post(`/api/user/forgot`, data)
}
const resetPassword = (data) => {
  return _post(`/api/user/reset`, data)
}
const updateInfo = (id, data) => {
  return _patch(`/api/user/update-user/${id}`, data)
}
const uploadAvatar = (data) => {
  return _post(`/api/upload-route/upload_avatar`, data)
}
const deleteInfo = (id) => {
  return _delete(`/api/user/delete/${id}`)
}
const updateRole = (id, role) => {
  return _patch(`/api/user/update-role/${id}`, role ? 1 : 0)
}
const getAllFlights = (data) => {
  return _post(`/api/flight/get-all`, data)
}
const getAllFlightsPaged = (data) => {
  return _post(`/api/flight/get-all-paged`, data)
}
const getAllFlightsNoPaged = (data) => {
  return _get(`/api/flight/get-all-no-paged`, data)
}
const createFlight = (data) => {
  return _post(`/api/flight/create`, data)
}
const deleteFlight = (data) => {
  return _post(`/api/flight/delete`, data)
}
const payPal = (data) => {
  return _post(`/api/flight/pay-pal`, data)
}
const getTickets = (data) => {
  return _post(`/api/flight/tickets`, data)
}
const createMessage = (data) => {
  return _post(`/api/message/create-message`, data)
}
const getMessage = () => {
  return _get(`/api/message/get-message`)
}
const deleteMessage = (data) => {
  return _post(`/api/message/delete-message`, data)
}

export {
	getToken,
  getUser,
  getAllUsers,
  getUserByGmail,
  getUserById,
  login,
  googleLogin,
  facebookLogin,
  register,
  activeEmail,
  forgotPassword,
  resetPassword,
  updateInfo,
  uploadAvatar,
  deleteInfo,
  updateRole,
  getAllFlights,
  getAllFlightsPaged,
  getAllFlightsNoPaged,
  createFlight,
  deleteFlight,
  payPal,
  getTickets, 
  createMessage,
  getMessage,
  deleteMessage,
}
