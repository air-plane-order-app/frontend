import io from 'socket.io-client';
import React from 'react';

export const socket = io("localhost:5001", {transports: ['websocket'], upgrade: false})
export const SocketContext = React.createContext();

// import io from 'socket.io-client';

// export class SocketService {

//   function init() {
//     console.log('initiating socket service');
//     socket = io('localhost:5001');
//     return this;
//   }

//   const onCodeChange = (code) => {
//     console.log('emitting code: ',code);
//     this.socket.emit('CODE_CHANGED', code);
//   }

//   cosnt onCodeListener = (editor) => {
//     this.socket.on('CODE_CHANGED', (code) => {
//       console.log('listening code: ',code);
//       editor(code.code)
//     })
//   }
// }

