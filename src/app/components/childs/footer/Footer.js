import './Footer.css'
function Footer() {
  return (
    <footer class="footer-distributed">

			<div class="footer-left">

				<h3>NK-UTE<span>Airline</span></h3>

				<p class="footer-links">
					<a href="#" class="link-1">Home</a>

					<a href="/support">Support</a>

					<a href="/contact">Contact</a>
				</p>

				<p class="footer-company-name">NK UTE Airline © 2021</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>1 Vo Van Ngan</span> Thu Duc City, HCM City</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>0768.185.158</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">khangvo@gmail.com</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>About the company</span>
					NK UTE Airline is a website to buy airline ticket.
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>

			</div>

		</footer>
  );
}
export default Footer;
