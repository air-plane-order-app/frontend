import { Link, useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Navbar.css';
import React, { useEffect, useState, useContext } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  isLoggedIn,
  updateUser,
  updateSupport,
} from '../../../../store/authReducer';
import { SocketContext } from '../../../context/socket/Socket';

function Navbar() {
  const socket = useContext(SocketContext);
  const cookies = new Cookies();
  const dispatch = useDispatch();
  const history = useHistory();
  const auth = useSelector((state) => state.auth);
  const isSupport = useSelector((state) => state.auth.isSupport);
  const user = useSelector((state) => state.auth.user);

  const [link, setLink] = useState(false);
  const [connect, setConnect] = useState(false);
  const [code, setCode] = useState(null);

  const { isLogged, isAdmin } = auth;

  useEffect(() => {
    console.log('authtt', auth);

    socket.on('needSupport', ({ author }) => {
      if (isAdmin) {
        dispatch(updateSupport(true));
      }
    });
  });

  const handleLogout = async () => {
    try {
      cookies.set('isLoggedIn', false, { path: '/' });
      cookies.set('token', null, { path: '/' });
      dispatch(updateUser({ user: null, isAdmin: false }));
      dispatch(isLoggedIn({ result: false, token: null }));
      dispatch(updateSupport(false));

      window.location.href = '/';
    } catch (err) {
      console.log(err);
    }
  };

  const enableLink = () => {
    setLink(!link);
  };

  const enableConnect = () => {
    setConnect(!connect);
  };

  const onCodeChange = (e) => {
    const value = e.target.value;

    setCode(value);
  }

  const joinBooking = () => {
    history.replace({
      pathname: '/booking',
      state: {
        shareCode: code,
      }
    })
  }

  const goHome = () => {
    window.location.href = '/'
  }

  const userLink = () => {
    return (
      <li onClick={enableLink} className="avatar-div">
        {isSupport && <div className="avatar-div-support" />}

        <Link to="#" className="avatar">
          <img src={user?.avatar} alt="" />
        </Link>
        <div
          style={{ ...(link ? { display: 'block' } : { display: 'none' }) }}
          className="drop-down"
        >
          <li>
            <Link to="/profile">Profile</Link>
          </li>
          <li>
            <Link to="/my-ticket">Tickets</Link>
          </li>
          <li>
            <Link to="/" onClick={handleLogout}>
              Logout
            </Link>
          </li>
        </div>
      </li>
    );
  };

  return (
    <header>
      <div className="logo">
        <Link to="#" onClick={goHome}>
          <img className="logo-img" src="https://i.ibb.co/hVrYcHn/NK.png"></img>
        </Link>
      </div>
      <ul>
        {isLogged && (
          <li>
            <Link to="#" onClick={enableConnect}>
              <i className="fas fa-shopping-cart"></i> Connect
            </Link>
            <div
              style={{ ...(connect ? { display: 'block' } : { display: 'none' }) }}
              className="drop-down"
            >
              <li className="share-code">
                <input value={code} class="form-control" type="text" onChange={onCodeChange} />
                <button onClick={joinBooking} className="btn btn-primary">JOIN</button>
              </li>
            </div>
          </li>
        )}
        <li>
          <Link to="/contact">
            <i className="fas fa-shopping-cart"></i> Contact
          </Link>
        </li>
        <li>
          <Link to="/">
            <i className="fas fa-shopping-cart"></i> Search
          </Link>
        </li>
        {isAdmin && isLogged && (
          <li>
            <Link to="/cms">
              <i className="fas fa-shopping-cart"></i> CMS
            </Link>
          </li>
        )}
        {!isAdmin && isLogged && (
          <li>
            <Link to="/support">
              <i className="fas fa-shopping-cart"></i> Support
            </Link>
          </li>
        )}
        {isLogged ? (
          userLink()
        ) : (
          <li>
            <Link to="/login">
              <i className="fas fa-user"></i> Sign in
            </Link>
          </li>
        )}
      </ul>
    </header>
  );
}
export default Navbar;
