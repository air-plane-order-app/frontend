import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import ActivationEmail from './pages/auth/ActivationEmail';
import NotFound from './childs/notfound/NotFound';
import ForgotPass from './pages/auth/ForgotPassword';
import ResetPass from './pages/auth/ResetPassword';
import Profile from './pages/profile/Profile';
import MyTicket from './pages/profile/MyTicket';
import EditUser from './pages/profile/EditUser';
import Homepage from './pages/homepage/Homepage';
import Contact from './pages/contact/Contact';
import Support from './pages/support/Support';
import Search from './pages/search/Search';
import Confirm from './pages/confirm/Confirm';
import {Booking} from './pages/booking/Booking';

import './pages/auth/auth.css';

export const WebRouter = () => {
  const auth = useSelector((state) => state.auth);
  const { isLogged, isAdmin } = auth;
  return (
    <section>
      <Switch>
        <Route path="/" component={Homepage} exact />

        <Route path="/login" component={isLogged ? NotFound : Login} exact />
        <Route
          path="/register"
          component={isLogged ? NotFound : Register}
          exact
        />
        <Route
          path="/search"
          // component={isLogged ? List : NotFound}
          component={Search}
          exact
        />
        <Route
          path="/confirm"
          // component={isLogged ? List : NotFound}
          component={Confirm}
          exact
        />
        <Route
          path="/forgot_password"
          component={isLogged ? NotFound : ForgotPass}
          exact
        />
        <Route path="/user/reset/:token" component={ResetPass} exact />

        <Route
          path="/user/activate/:activation_token"
          component={ActivationEmail}
          exact
        />

        <Route
          path="/profile"
          component={isLogged ? Profile : NotFound}
          exact
        />
        <Route
          path="/my-ticket"
          component={isLogged ? MyTicket : NotFound}
          exact
        />
        <Route
          path="/edit_user/:id"
          component={isAdmin ? EditUser : NotFound}
          exact
        />
        <Route path="/contact" component={Contact} exact />
        <Route
          path="/support"
          render={(...props) => <Support {...props} key={Math.random()} />}
          exact
        />
        <Route
          path="/booking"
          render={(...props) => <Booking {...props} />}
          exact
        />
      </Switch>
    </section>
  );
};

export default WebRouter;
