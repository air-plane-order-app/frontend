import React, { useState } from 'react';
import { GoogleMap, withScriptjs, withGoogleMap } from 'react-google-maps';
import './contact.css';
import airports from '../../../../airport-data.json';

import 'bootstrap/dist/css/bootstrap.min.css';

const iframes = {
  VCS: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4498.735593597812!2d106.62818542529723!3d8.72779624838135!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x92661eb6b91f77ec!2zUGhpIFRyxrDhu51uZyBD4buPIOG7kG5nICh4xrBhKQ!5e0!3m2!1sen!2s!4v1639147122732!5m2!1sen!2s",
  CAH: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3938.7616232104488!2d105.17350881591382!3d9.175962893419!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a14855e8270d83%3A0xe49b2677bfa4fcdc!2sCa%20Mau%20Airport!5e0!3m2!1sen!2s!4v1639147222876!5m2!1sen!2s",
  VKG: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.684417032952!2d105.1322981159174!3d9.960192892878505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a0b5d52d55cc43%3A0x874ec8f86b3a1640!2sRach%20Gia%20Airport!5e0!3m2!1sen!2s!4v1639147303645!5m2!1sen!2s"
}

function Map() {
  return (
    <GoogleMap
      defaultZoom={10}
      defaultCenter={{ lat: 10.817196, lng: 106.658433 }}
    />
  );
}

const WrappedMap = withScriptjs(withGoogleMap(Map));

const Contact = () => {
  const [iframe, setIframe] = useState('');

  const onChange = (e) => {
    setIframe(iframes[e.target.value]);
  }

  return (
    <div style={{ padding: '50px 270px' }}>
      <h3>MAP</h3>
      <div className="col-md-12">
        <div className="form-group">
          <select
            className="mdb-select md-form form-control"
            onChange={onChange}
            searchable="Search here.."
          >
            <option value="" disabled selected>
              Choose an airport
            </option>
            {airports.map((item) => {
              return (
                <option key={item.Code} value={item.Code}>
                  {item.Code} - {item.name}
                </option>
              );
            })}
          </select>
        </div>
      </div>
      <div>
        {/* <WrappedMap
          googleMapURL={`https://maps.googleapis.com/maps/api/js?key=AIzaSyAU-Tj9iraGUI-dh-pPPtT_EiO9tQHVOac&v=3.exp&libraries=geometry,drawing,places`}
          loadingElement={<div style={{ height: '100%' }} />}
          containerElement={<div style={{ height: `100%` }} />}
          mapElement={<div style={{ height: `100%` }} />}
        /> */}
        <iframe src={iframe} height="500px" width="100%" title="Iframe Example"></iframe>
      </div>
    </div>
  );
}


export default Contact
