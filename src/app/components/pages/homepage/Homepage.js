import React, { useState } from 'react';
import './Homepage.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from "../../childs/footer/Footer"
import {useHistory} from 'react-router-dom'
import airports from '../../../../airport-data.json'
import moment from 'moment';

const iconList = [
  {
    from: 'Từ Hà Nội',
    to: 'Đến TP. Hồ Chí Minh',
    img: 'https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/49vn-1621505158847.png',
  },
  {
    from: 'Từ TP. Hồ Chí Minh',
    to: 'Đến Đà Lạt',
    img: 'https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/39vn-1621505224345.png',
  },
  {
    from: 'Từ Hà Nội',
    to: 'Đến Đà Lạt',
    img: 'https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/99vn-1621505224342.png',
  },
  {
    from: 'Từ TP. Hồ Chí Minh',
    to: 'Đến Đà Nẵng',
    img: 'https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/39vn-1621505224345.png',
  },
]

const cardList = [
  {
    img: 'https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/baohiem-1620115781619.jpg',
    title: 'An tâm trọn vẹn',
    description: [
      {
        text: 'Các chương trình bảo hiểm uy tín đến từ các đối tác bảo hiểm tin cậy của Vietjet'
      },
      {
        text: 'Quyền lợi bảo hiểm đa dạng, hấp dẫn'
      },
    ],
  },
  {
    img: 'https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/files/uutiencheckin-1592284373823.png',
    title: 'Nhanh chóng, tiết kiệm thời gian',
    description: [
      {
        text: 'Làm thủ tục nhanh nhất tại quầy SkyBoss'
      },
      {
        text: 'Giảm thời gian xếp hàng, chờ đợi'
      },
    ],
  },
  {
    img: 'https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/files/swift247-1592284169014.png',
    title: 'Vận chuyển Bắc - Trung - Nam siêu tốc, siêu tiện lợi',
    description: [
      {
        text: 'Rút ngắn khoảng cách hàng nghìn km trong thời gian ngắn nhất'
      },
      {
        text: 'Đặt đơn, gửi và nhận hàng 24/7'
      },
    ],
  },
]

const cardPlaces = [
  {
    img: 'https://dulichkhampha24.com/wp-content/uploads/2020/12/kinh-nghiem-du-lich-quang-binh-c.jpg',
    title: 'Quảng Bình'
  },
  {
    img: 'https://upload.wikimedia.org/wikipedia/commons/9/9c/Chi%E1%BB%81u_cao_nguy%C3%AAn_-_Late_afternoon_in_the_Central_High_Plateaux_-_panoramio.jpg',
    title: 'Gia Lai'
  },
  {
    img: 'https://static3.thetravelimages.com/wordpress/wp-content/uploads/2021/07/View-Of-Coastal-Resort-Phu-Quoc.png',
    title: 'Phú Quốc'
  },
  {
    img: 'https://s.meta.com.vn/img/thumb.ashx/Data/image/2021/08/25/dan-so-thanh-hoa-1.jpg',
    title: 'Thanh Hoá'
  },
  {
    img: 'https://cdn.kinhtedothi.vn/2019/03/19/TPVING.jpg',
    title: 'Nghệ An'
  },
  {
    img: 'https://dulichkhampha24.com/wp-content/uploads/2019/09/kinh-nghiem-du-lich-Ha-Noi-1.jpg',
    title: 'Hà Nội'
  },
]

const TYPE = {
  ONE_WAY: 0,
  ROUNDTRIP: 1,
}

export const Homepage = () => {
  const history = useHistory();

  const [departure, setDeparture] = useState(null);
  const [arrival, setArrival] = useState(null);
  const [departureTime, setDepartureTime] = useState(null);
  const [arrivalTime, setArrivalTime] = useState(null);
  const [adults, setAdults] = useState(0);
  const [children, setChildren] = useState(0);
  const [type, setType] = useState(TYPE.ROUNDTRIP);

  const onSubmit = () => {
    history.push({
      pathname: '/search',
      state: {
        departureFromLocation: departure,
        departureToLocation: arrival,
        departureStartTime: moment(new Date(departureTime)).toISOString(),
        arrivalStartTime: !!arrivalTime && moment(new Date(arrivalTime)).toISOString(),
        type: type == TYPE.ROUNDTRIP ? 'roundtrip' : 'oneway',
        totalSeat: parseInt(children) + parseInt(adults),
      }
    })
  }

  const onDropDownChange = (e, type = 'departure') => {
    switch(type) {
      case 'departure':
        setDeparture(e.target.value)
        break;
      case 'arrival':
        setArrival(e.target.value)
        break;
      case 'departureTime':
        setDepartureTime(e.target.value)
        break;
      case 'arrivalTime':
        setArrivalTime(e.target.value)
        break;
      case 'adults':
        setAdults(e.target.value)
        break;
      case 'children':
        setChildren(e.target.value)
        break;
    }

    // console.log(departure, arrival, moment(new Date(departureTime)).toISOString(), arrivalTime, adults, children)
  }

  const onTypeChange = (e, type) => {
    if (type === 'roundtrip') {
      setType(TYPE.ROUNDTRIP)
    } else {
      setType(TYPE.ONE_WAY)
      setArrivalTime(null)
    }
  }

  return (
    <div>
      <div id="booking" className="section">
        <div className="section-center">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="booking-cta">
                  <h1>NK-UTE Airline</h1>
                  <p>
                    Đi đến bất cứ đâu với dịch vụ của chúng tôi. Còn chờ gì nữa
                    mà không đặt vé với những ưu đãi cực hấp dẫn!!!
                  </p>
                </div>
              </div>
              <div className="col-md-7 col-md-offset-1">
                <div className="booking-form">
                  <form>
                    <div className="form-group">
                      <div className="form-checkbox">
                        <label htmlFor="roundtrip">
                          <input
                            type="radio"
                            id="roundtrip"
                            name="flight-type"
                            checked={type == TYPE.ROUNDTRIP}
                            onChange={(e) => onTypeChange(e, 'roundtrip')}
                          />
                          <span></span>Roundtrip
                        </label>
                        <label htmlFor="one-way">
                          <input type="radio" id="one-way" name="flight-type" checked={type == TYPE.ONE_WAY} onChange={(e) => onTypeChange(e, 'one-way')}/>
                          <span></span>One way
                        </label>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <span className="form-label">Flying from</span>
                          <select className="mdb-select md-form form-control" onChange={(e) => onDropDownChange(e, 'departure')} searchable="Search here..">
                            <option value="" disabled selected>Choose your country</option>
                            {airports.map((item) => {
                              return (<option key={item.Code} value={item.Code}>{item.Code} - {item.name}</option>)
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <span className="form-label">Flying to</span>
                          <select className="mdb-select md-form form-control" onChange={(e) => onDropDownChange(e, 'arrival')} searchable="Search here..">
                            <option value="" disabled selected>Choose your country</option>
                            {airports.map((item) => {
                              return (<option key={item.Code} value={item.Code}>{item.Code} - {item.name}</option>)
                            })}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <span className="form-label">Departing</span>
                          <input onChange={(e) => onDropDownChange(e, 'departureTime')} className="form-control" type="date" required />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <span className="form-label">Returning</span>
                          <input onChange={(e) => onDropDownChange(e, 'arrivalTime')} className={type === TYPE.ONE_WAY ? 'form-control oneway' : 'form-control'} type="date" required disabled={type === TYPE.ONE_WAY}/>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <span className="form-label">Adults (18+)</span>
                          <select onChange={(e) => onDropDownChange(e, 'adults')} className="form-control">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                          </select>
                          <span className="select-arrow"></span>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <span className="form-label">Children (0-17)</span>
                          <select onChange={(e) => onDropDownChange(e, 'children')} className="form-control">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                          </select>
                          <span className="select-arrow"></span>
                        </div>
                      </div>
                    </div>
                    <div className="form-btn">
                      <button className="submit-btn" onClick={onSubmit}>Show flights</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="ads">
        <div className="images-container">
          <img className="images" src="https://en.nhandan.vn/cdn/en/media/k2/items/src/928/5a53bcb60619e14c13a8ee345f915e44.jpg" />
          <img className="images" src="https://blog.aci.aero/wp-content/uploads/2019/03/shutterstock_745544935.jpg" />
        </div>

        <div className="icon-list">
          {iconList.map((item) => {
            return (
              <div className="icon-list-container">
                <div className="icon-list-from">{item.from}</div>
                <img className="icon-list-img" src={item.img}/>
                <div className="icon-list-to">{item.to}</div>
              </div>
            )
          })}
        </div>
      </div>

      <div id="banner-1">
          <img className="banner-img"src="https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/vietjetclassichoantienmuasambanner1200x240-1636708454164.jpg"/>
      </div>

      <div id="card-banner">
          {cardList.map((item) => {
            return (
              <div className="card">
                <img className="card-img" src={item.img}/>
                <div className="card-content">
                  <div className="card-title">{item.title}</div>
                  <div>
                    {item.description.map((i) => {
                      return <div key={i}>{i.text}</div>;
                    })}
                  </div>
                </div>
              </div>
            )
          })}
      </div>

      <div id="banner-1">
          <img className="banner-img"src="https://vj-prod-website-cms.s3.ap-southeast-1.amazonaws.com/files/shopping1-1598436856424.jpg"/>
      </div>

      <div id="card-place">
        {cardPlaces.map((i) => {
          return (
            <div className="card-place-item">
              <img className="card-place-img" src={i.img}/>
              <div className="card-place-title">{i.title}</div>
            </div>
          )
        })}
      </div>

      <Footer></Footer>
    </div>

  );
};

export default Homepage;
