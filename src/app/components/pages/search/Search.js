import React, { useEffect } from 'react';
import useState from 'react-usestateref';
import './Search.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from '../../childs/footer/Footer';
import { ReactComponent as LocationIcon } from './assets/location.svg';
import { ReactComponent as Airplane } from './assets/airplane.svg';
import { useLocation } from 'react-router-dom';
import { getAllFlights, getAllFlightsPaged } from '../../../core/apis';
import { useHistory } from 'react-router-dom';
import airports from '../../../../airport-data.json';
import moment from 'moment';
import { useSelector } from 'react-redux';

const TICKET = {
  eco: 'Eco Class',
  business: 'Business Class',
  first: 'First Class',
};

export const Search = () => {
  const history = useHistory();
  const location = useLocation();
  const isLogged = useSelector((state) => state.auth.isLogged);

  const ticketsData = {
    departureFromLocation: location.state.departureFromLocation,
    departureToLocation: location.state.departureToLocation,
    departureStartTime: location.state.departureStartTime,
    arrivalStartTime: location.state.arrivalStartTime,
    type: location.state.type,
    totalSeat: location.state.totalSeat,
    limitPage: 3,
    skipPage: 0,
  };
  var [formData, setFormData, ref] = useState([]);

  const [page, setPage] = useState(1);
  const [ticketData, setTicketData] = useState(ticketsData);
  const [ticketId, setTicketId] = useState(null);
  const [dataLength, setDataLength] = useState(0);
  const [departureFromLocation, setDepartureFromLocation] = useState("City A");
  const [departureToLocation, setDepartureToLocation] = useState("City B");
  const [departureStartTime, setDepartureStartTime] = useState("Start");
  const [departureEndTime, setDepartureEndTime] = useState("End");
  const [classTicket, setClassTicket] = useState("Class");
  const [price, setPrice] = useState(0);

  const onSubmit = () => {
    if (!isLogged) {
      history.replace('/login')
      return;
    }

    history.replace({
      pathname: '/booking',
      state: {
        id: ticketId,
        departureFromLocation: departureFromLocation,
        departureToLocation: departureToLocation,
        departureStartTime: departureStartTime,
        departureEndTime: departureEndTime,
        totalSeat: location.state.totalSeat,
        classTicket: classTicket,
        price: price,
      },
    });
  };
  const handleChangeInput = (e, data, type) => {
    setTicketId(data?._id)
    setDepartureFromLocation(
      airports.find((item) => item.Code == data?.departureFromLocation).name,
    );
    setDepartureToLocation(
      airports.find((item) => item.Code == data?.departureToLocation).name,
    );
    setDepartureStartTime(
      moment(new Date(data?.departureStartTime)).format('DD/MM/YYYY'),
    );
    setDepartureEndTime(
      moment(new Date(data?.departureEndTime)).format('DD/MM/YYYY'),
    );
    setClassTicket(TICKET[type]);
    setPrice(
      type === 'eco'
        ? data?.ecoClass
        : type === 'business'
        ? data?.businessClass
        : data?.firstClass,
    );
  };
  const nextPage = async () => {
    const pg = page < Math.ceil(dataLength / 3) ? page + 1 : page;
    setTicketData({...ticketData, skipPage: (pg - 1) * 3});
    const result = await getAllFlightsPaged({...ticketData, skipPage: (pg - 1) * 3});
    setFormData(result?.data?.flight ?? []);
    setPage(pg);
  };
  const prevPage = async () => {
    const pg = page === 1 ? 1 : 1;
    setTicketData({...ticketData, skipPage: (pg - 1) * 3});
    const result = await getAllFlightsPaged({...ticketData, skipPage: (pg - 1) * 3});
    setFormData(result?.data?.flight ?? []);
    setPage(pg);
  };

  useEffect(() => {
    (async () => {
      const result = await getAllFlightsPaged(ticketData);
      const dataLength = await getAllFlights(location.state);
      setDataLength(dataLength?.data?.flight?.length)
      setFormData(result?.data?.flight ?? []);
    })();
  }, [location]);

  const renderRadio = (item, data, type) => (
    <div class="form-check">
      <input
        class="form-check-input"
        type="radio"
        name="flexRadioDefault"
        id="flexRadioDefault1"
        onChange={(e) => handleChangeInput(e, data, type)}
      />
      <label class="form-check-label" for="flexRadioDefault1">
        {item}
      </label>
    </div>
  );

  return (
    <div>
      <div id="list" class="section">
        <div className="title">
          <div className="title-text">
            Flight:{' '}
            {ref?.current && ref?.current.length > 0 && ref?.current[0].type} |
            Adults: {location.state?.totalSeat}
          </div>
          <div className="location">
            <div className="location-start">
              <LocationIcon className="location-icon" />
              <div className="location-string">
                Departure: {location.state.departureFromLocation}
              </div>
            </div>
            <div className="location-des">
              <LocationIcon className="location-icon" />
              <div className="location-string">
                Arrival: {location.state.departureToLocation}
              </div>
            </div>
          </div>
        </div>

        <div className="information">
          <div className="information-table row">
            <table className="table col">
              <thead>
                <tr>
                  <th scope="col">Departure Place</th>
                  <th scope="col">Arrival Place</th>
                  <th scope="col">Departure Time</th>
                  <th scope="col">Arrival Time</th>
                  <th scope="col">Available Seat</th>
                  <th scope="col">Eco Class</th>
                  <th scope="col">Business Class</th>
                  <th scope="col">First Class</th>
                </tr>
              </thead>
              <tbody>
                {formData.map((item) => {
                  return (
                    <tr key={item.Code}>
                      <td>
                        {
                          airports.filter(
                            (i) => i.Code == item.departureFromLocation,
                          )[0]?.name
                        }
                      </td>
                      <td>
                        {
                          airports.filter(
                            (i) => i.Code == item.departureToLocation,
                          )[0]?.name
                        }
                      </td>
                      <td>{item?.flightStartTime}</td>
                      <td>{item?.flightEndTime}</td>
                      <td>{item?.availableSeat}</td>

                      <td>{renderRadio(item.ecoClass, item, 'eco')}</td>
                      <td>
                        {renderRadio(item.businessClass, item, 'business')}
                      </td>
                      <td>{renderRadio(item.firstClass, item, 'first')}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div className="page-number">
              <button className="page-btn" onClick={prevPage}>
                Prev
              </button>
              <label>
                {'<'} {page} {'>'}
              </label>
              <button className="page-btn" onClick={nextPage}>
                Next
              </button>
            </div>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Booking Information</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div className="flight-info">
                      <div className="flight-title">Departure Flight</div>

                      <div className="flight-description">
                        <div>{departureFromLocation}</div>
                        <Airplane className="airplane" />
                        <div>{departureToLocation}</div>
                      </div>

                      <div>Departure Time: {departureStartTime}</div>
                      <div>Arrival Time: {departureEndTime}</div>
                      <div>Class: {classTicket}</div>
                      <div>Tax: 10%</div>
                      <div>
                        <td className="price-container">
                          <div>Price: </div>
                          <div className="price-secondary">{price} VND</div>
                        </td>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="price-container-secondary">
                    <div className="price-container">
                      <div className="flight-title">Total Price: </div>
                      <div className="price">
                        {price * location.state.totalSeat + (price * 10) / 100}{' '}
                        VND
                      </div>
                    </div>
                    <div className="continue">
                      <button
                        type="button"
                        class="btn btn-primary"
                        onClick={onSubmit}
                      >
                        Continue
                      </button>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default Search;
