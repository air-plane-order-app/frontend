import React, {useState} from 'react'
import {isEmail} from '../../childs/validation/Validation'
import {showErrMsg, showSuccessMsg} from '../../childs/notifications/Notification'
import { forgotPassword } from '../../../core/apis';

const initialState = {
    email: '',
    err: '',
    success: ''
}

function ForgotPassword() {
    const [data, setData] = useState(initialState)

    const {email, err, success} = data

    const handleChangeInput = e => {
        const {name, value} = e.target
        setData({...data, [name]:value, err: '', success: ''})
    }

    const onSubmit = async () => {
        if(!isEmail(email))
            return setData({...data, err: 'Invalid emails.', success: ''})

        try {
            const res = await forgotPassword({email})

            return setData({...data, err: '', success: res.data.msg})
        } catch (err) {
            err.response.data.msg && setData({...data, err:  err.response.data.msg, success: ''})
        }
    }

    return (
        <div className="fg_pass">
            <h2>Forgot Your Password?</h2>

            <div className="row">
                {err && showErrMsg(err)}
                {success && showSuccessMsg(success)}

                <label htmlFor="email">Enter your email address</label>
                <input type="email" name="email" id="email" value={email}
                onChange={handleChangeInput} />
                <button onClick={onSubmit}>Verify your email</button>
            </div>
        </div>
    )
}

export default ForgotPassword
