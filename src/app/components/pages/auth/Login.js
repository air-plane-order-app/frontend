import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';
import {
  showErrMsg,
  showSuccessMsg,
} from '../../childs/notifications/Notification';
import { useDispatch } from 'react-redux';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import { login, googleLogin, facebookLogin } from '../../../core/apis';
import { isLoggedIn, updateUser } from '../../../../store/authReducer';

const initialState = {
  email: '',
  password: '',
  err: '',
  success: '',
};
function Login() {
  const [user, setUser] = useState(initialState);
  const dispatch = useDispatch();
  const history = useHistory();
  const cookies = new Cookies();

  const { email, password, err, success } = user;

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value, err: '', success: '' });
  };

  const handleStoreData = (data, info) => {
    cookies.set('isLoggedIn', true, { path: '/' });
    cookies.set('token', data.tokens, data.user?.role?.roleName);
    dispatch(updateUser({user: data.user, isAdmin: data.user?.role?.roleName === 'admin'}));
    dispatch(isLoggedIn({ result: true, token: data.tokens }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await login({ email, password });
      setUser({ ...user, err: '', success: res.data.msg });
      handleStoreData(res.data, email);

      history.replace('/');
    } catch (err) {
      err?.response?.data?.msg &&
        setUser({ ...user, err: err.response?.data?.msg, success: '' });
    }
  };

  const responseGoogle = async (response) => {
    try {
      console.log('response', response)
      const res = await googleLogin({ tokenId: response.tokenId });
      setUser({ ...user, error: '', success: res.data.msg });
      handleStoreData(res.data);
      history.replace('/');
    } catch (err) {
      !err?.response?.data?.msg &&
        setUser({ ...user, err: err.response?.data?.msg, success: '' });
    }
  };

  const responseFacebook = async (response) => {
    try {
      const { accessToken, userID } = response;
      const res = await facebookLogin({ accessToken, userID });

      setUser({ ...user, error: '', success: res.data.msg });
      handleStoreData(res.data);

      history.replace('/');
    } catch (err) {
      err?.response?.data?.msg &&
        setUser({ ...user, err: err.response?.data?.msg, success: '' });
    }
  };
  return (
    <div className="body">
      <div className="login_page">
        <h2>Login</h2>
        {err && showErrMsg(err)}
        {success && showSuccessMsg(success)}

        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="email">Email Address</label>
            <input
              type="text"
              placeholder="Enter email address"
              id="email"
              value={email}
              name="email"
              onChange={handleChangeInput}
            />
          </div>

          <div>
            <label htmlFor="password">Password</label>
            <input
              type="password"
              placeholder="Enter password"
              id="password"
              value={password}
              name="password"
              onChange={handleChangeInput}
            />
          </div>

          <div className="row">
            <button type="submit">Login</button>
            <Link to="/forgot_password">Forgot your password?</Link>
          </div>
        </form>

        <div className="hr">Or Login With</div>

        <div className="social">
          <GoogleLogin
            clientId="636249600778-1unevr20o6jrtg6q7a57tsbk3pgalmsn.apps.googleusercontent.com"
            buttonText="Login with google"
            onSuccess={responseGoogle}
            cookiePolicy={'single_host_origin'}
          />

          <FacebookLogin
            appId="417952863320032"
            autoLoad={false}
            fields="name,email,picture"
            callback={responseFacebook}
          />
        </div>
        <p>
          New Customer? <Link to="/register">Register</Link>
        </p>
      </div>
    </div>
  );
}

export default Login;
