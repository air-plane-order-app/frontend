import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './profile.css';
import moment from 'moment';

import {
  CalendarToday,
  LocationSearching,
  MailOutline,
  PermIdentity,
  PhoneAndroid,
  Publish,
} from '@material-ui/icons';
import { useHistory} from 'react-router-dom';
import '../../../features/CMS/pages/user/user.css';
import { updateInfo, getTickets } from '../../../core/apis';

function Profile() {
  const user = useSelector((state) => state.auth.user);
  const history = useHistory();

  const [data, setData] = useState([]);
  const [title, setTitle] = useState('Update');
  const [toggle, setToggle] = useState(false);
  const [firstInfo, setFirstInfo] = useState(user);
  
  const TYPE = {
    roundtrip: 'Khứ hồi',
    oneway: 'Một chiều',
  };

  useEffect(() => {
    (async () => {
      const res = await getTickets();
      if (res.data?.flight) {
        // console.log('hihihi',res?.data?.flight)
        const list = res.data.flight?.map((item) => {
          return {
            type: item?.flight?.type,
            from: item?.flight?.departureFromLocation,
            to: item?.flight?.departureToLocation,
            start: moment(new Date(item?.flight?.departureStartTime)).format(
              'DD-MM-YYYY',
            ),
            price: `${item?.totalPrice} VND`,
          };
        });

        setData(list);
      }
    })();
  }, []);

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setFirstInfo({ ...firstInfo, [name]: value });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await updateInfo( user?._id, firstInfo );
      console.log(res?.data)
    } catch (err) {
      err?.response?.data?.msg &&
      setFirstInfo({ ...firstInfo});
    }
    history.push('/profile')
  };

  const updateFile = async (e) => {
    const src = URL.createObjectURL(e.target.files[0])
    const base64 = await getBase64ImageFromUrl(src);
    setFirstInfo({
      ...firstInfo,
      avatar: base64,
    })
  }

  const getBase64ImageFromUrl = async (imageUrl) => {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }

  const onUpdate = () => {
    if (title === 'Update') setTitle('History')
    else setTitle('Update')
    setToggle(!toggle)
  }

  return (
    <body>
       <div className="profile_page">
        <div className="profile">
          <div className="profile-info">
            <div className="avatar-profile-div">
              <img className="avatar-img" src={user?.avatar} />
            </div>
            <div>First Name: {user?.name.split(' ')[0]}</div>
            <div>Last Name: {user?.name.split(' ')[1]}</div>
            <div>Email: {user?.email} </div>
            <div>Phone: {user?.phone} </div>
            <div>Address: {user?.address}</div>
            <div>ID Number: {user?.idNumber}</div>
            <div>Birth Date: {user?.birthDate}</div>
            <div className="update-btn-div">
              <button onClick={onUpdate} type="submit" className="update-btn">
               {title}
              </button>
            </div>
          </div>
        </div>

        <div className="ticket">
          {!toggle && (<div>
            <h3>My History</h3>
            <div className="ticket-table">
              <table>
                <thead>
                  <tr>
                    <th>Type</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Start</th>
                    <th>Price</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((item) => {
                    return (
                      <tr>
                        <td>{TYPE[item.type]}</td>
                        <td>{item.from}</td>
                        <td>{item.to}</td>
                        <td>{item.start}</td>
                        <td>{item.price}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>)}


          {toggle && (<div className="userContainer">
            <div className="userShow">
              <div className="userShowTop">
                <img
                  src={firstInfo?.avatar}
                  alt=""
                  className="userShowImg"
                />
                <div className="userShowTopTitle">
                  <span className="userShowUsername">{user?.name}</span>
                  <span className="userShowUserTitle">
                    {user?.idNumber}
                  </span>
                </div>
              </div>
              <div className="userShowBottom">
                <span className="userShowTitle">Account Details</span>
                <div className="userShowInfo">
                  <PermIdentity className="userShowIcon" />
                  <span className="userShowInfoTitle">
                    {user?.birthDate}
                  </span>
                </div>
                <div className="userShowInfo">
                  <PhoneAndroid className="userShowIcon" />
                  <span className="userShowInfoTitle">{user?.phone}</span>
                </div>
                <div className="userShowInfo">
                  <LocationSearching className="userShowIcon" />
                  <span className="userShowInfoTitle">{user?.address}</span>
                </div>
              </div>
            </div>
            <div className="userUpdate">
              <span className="userUpdateTitle">Edit</span>
              <form className="userUpdateForm" onSubmit={handleSubmit}>
                <div className="userUpdateLeft">
                  <div className="userUpdateItem">
                    <label>Full Name</label>
                    <input
                      type="text"
                      placeholder="Enter Your Name"
                      className="userUpdateInput"
                      name="name"
                      onChange={handleChangeInput}
                    />
                  </div>
                  <div className="userUpdateItem">
                    <label>Phone</label>
                    <input
                      type="text"
                      placeholder="Enter Your Phone"
                      className="userUpdateInput"
                      name="phone"
                      onChange={handleChangeInput}
                    />
                  </div>
                  <div className="userUpdateItem">
                    <label>Address</label>
                    <input
                      type="text"
                      placeholder="Enter Your Address"
                      className="userUpdateInput"
                      name="address"
                      onChange={handleChangeInput}
                    />
                  </div>
                  <div className="userUpdateItem">
                    <label>ID Number</label>
                    <input
                      type="text"
                      placeholder="Enter Your ID Number"
                      className="userUpdateInput"
                      name="idNumber"
                      onChange={handleChangeInput}
                    />
                  </div>
                  <div className="userUpdateItem">
                    <label>Birth Date</label>
                    <input
                      type="text"
                      placeholder="Enter Your Birth Date"
                      className="userUpdateInput"
                      name="birthDate"
                      onChange={handleChangeInput}
                    />
                  </div>
                </div>
                <div className="userUpdateRight">
                  <div className="userUpdateUpload">
                    <img
                      className="userUpdateImg"
                      src={firstInfo?.avatar}
                      alt=""
                    />
                    <label htmlFor="file">
                      <Publish className="userUpdateIcon" />
                    </label>
                    <input
                      onChange={updateFile}
                      type="file"
                      id="file"
                      style={{ display: 'none' }}
                    />
                  </div>
                  <button className="userUpdateButton" type="submit">
                    Update
                  </button>
                </div>
              </form>
            </div>
          </div>)}
        </div>
      </div>
    </body>
  );
}

export default Profile;
