import React, { useState, useEffect } from 'react';
import './profile.css';
import moment from 'moment';
import {getTickets} from '../../../core/apis'

const TYPE = {
  roundtrip: 'Khứ hồi',
  oneway: 'Một chiều',
};

const MyTicket = () => {
  const [data, setData] = useState([])

  const cancel = (e) => {
    e.preventDefault();
    alert('Cancelled')
  }

  useEffect(() => {
    (async () => {
      const res = await getTickets();
      if (res.data?.flight) {
        // console.log('hihihi',res?.data?.flight)
        const list = res.data.flight?.map(item => {
          return {
            type: item?.flight?.type,
            from: item?.flight?.departureFromLocation,
            to: item?.flight?.departureToLocation,
            start: moment(new Date(item?.flight?.departureStartTime)).format('DD-MM-YYYY'),
            price: `${item?.totalPrice} VND`,
          }
        })

        setData(list);
      }
    })()
  },[])

  return (
    <body>
      <div className="my-ticket">
        <h3>My Tickets</h3>
        <div className="my-ticket-table">
          <table>
            <thead>
              <tr>
                <th>Type</th>
                <th>From</th>
                <th>To</th>
                <th>Start</th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {data.map((item) => {
                return (
                  <tr>
                    <td>{TYPE[item.type]}</td>
                    <td>{item.from}</td>
                    <td>{item.to}</td>
                    <td>{item.start}</td>
                    <td>{item.price}</td>
                    <td>
                      <div>
                        <input onClick = {cancel} type = "button" value = "Cancel"></input>
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </body>
  );
}

export default MyTicket;
