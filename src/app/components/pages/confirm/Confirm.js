import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from '../../childs/footer/Footer';
import { useHistory } from 'react-router-dom';
import { payPal } from '../../../core/apis';
import { useLocation } from 'react-router-dom';
import { ReactComponent as LocationIcon } from './assets/location.svg';
import { ReactComponent as Airplane } from './assets/airplane.svg';
import './Confirm.css';

export const Confirm = () => {
  const history = useHistory();
  const location = useLocation();
  const [data, setData] = useState([]);

  const onSubmit = () => {
    history.replace('/my-ticket');
  };

  useEffect(() => {
    console.log("confirm:", location.state);
    setData(location.state);
  }, [location]);

  const onPaypalSubmit = () => {
    (async () => {
      const res = await payPal(data);
      console.log(res.data)
      if (res.data) {
        window.location.href = res.data;
      }
    })()
  }

  return (
    <>
      <div>
        <div className="confirm-container">
          <div className="confirm-container-secondary">
            <h3>Xác nhận đơn hàng</h3>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Thông tin đặt chổ</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div className="flight-info">
                      <div className="flight-title">Chuyến đi</div>
                     <div className="flight-description">
                        <div>{data.departureFromLocation}</div>
                        <Airplane className="airplane" />
                        <div>{data.departureToLocation}</div>
                      </div>

                      <div>Departure Time: {data.departureStartTime}</div>
                      <div>Arrival Time: {data.departureEndTime}</div>
                      <div>Total Seat: {data.totalSeat}</div>
                      <div>Class: {data.classTicket}</div>
                      <div>Tax: 10%</div>
                      <div>
                        <td className="price-container">
                          <div>Price: </div>
                          <div className="price-secondary">{data.price} VND</div>
                        </td>
                      </div>
                    </div>
                    <div class = "return">
                      <p>Có thể hoàn trả trước 02/12/2021</p>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div className="pay-container">
            <h3>Thanh toán</h3>
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Thông tin thanh toán</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div className="flight-info">
                      <div className="pay-input">
                        <select className = "select-container">
                          <option>
                            ACB - Ngân hàng thương mại cổ phần Á Châu
                          </option>
                          <option>
                            TCB - Ngân hàng thương mại cổ phần Kỹ Thương Việt
                            Nam
                          </option>
                          <option>
                            SCB - Ngân hàng Thương mại cổ phần Sài Gòn
                          </option>
                        </select>
                      </div>
                      <div>
                        <input className="pay-input" placeholder="Số tài khoản" />
                      </div>
                      <div >
                        <input className="pay-input" placeholder="Ngày cấp" />
                      </div>
                      <div>
                        <input className="pay-input" disabled = "true" placeholder="Họ tên" />
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="price-container-secondary">
                    <div className="price-container">
                      <div className="flight-title">Tổng tiền: </div>
                      <div className="price">{data.price * data.totalSeat + (data.price * 10) / 100}{' '}
                       VND</div>
                    </div>
                    <div className="continue">
                      <button
                        type="button"
                        class="btn btn-primary"
                        onClick={onSubmit}
                      >
                        Thanh toán
                      </button>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td className="price-container-secondary">
                    <div className="price-container">
                      <div className="flight-title">PayPal: </div>
                      <div className="price">
                        {Math.round((data.price * data.totalSeat + (data.price * 10) / 100) / 22910)}{' '}
                        USD
                      </div>
                    </div>
                    <div className="continue">
                      <button
                        type="button"
                        class="btn btn-primary"
                        onClick={onPaypalSubmit}
                      >
                        Continue with PayPal
                      </button>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </>
  );
};

export default Confirm;
