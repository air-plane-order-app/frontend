import React, { useEffect, useState, useContext } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './Support.css';
import { createMessage } from '../../../core/apis';
import { SocketContext } from '../../../context/socket/Socket';

const roomId = btoa(Math.random().toString() + new Date().getTime().toString());

const Support = () => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const socket = useContext(SocketContext);

  const { isAdmin, user } = auth;

  const [data, setData] = useState({
    messages: [
      {
        message: 'Welcome! Type "OK" to get support from Admin',
        author: 'Bot',
      },
    ],
    input: '',
  });
  

  useEffect(() => {
    
    (async () => {
      console.log('ok')
      const res = await createMessage({ name: user?.name, room: roomId });
      setTimeout(() => {
        console.log(socket)
      }, 3000)
      socket.emit('joinRoom', { name: user?.name, room: roomId });
    })();

    return () => {
      socket.disconnect();
    };
  }, [roomId]);

  useEffect(() => {
    socket.on('message', ({ author, message }) => {
      console.log(`${author}: `, message);

      let messages = [...data.messages];

      messages.push({ author, message });

      setData({
        ...data,
        messages: messages,
      });
    });
  });

  const updateInput = (e) => {
    setData({
      ...data,
      input: e.target.value,
    });
  };

  const handleMessage = () => {
    const author = isAdmin ? 'Admin' : user?.name;

    if (data.input !== '') {
      socket.emit('message', {
        message: data.input,
        author: author,
        room: roomId,
      });

      setData({
        ...data,
        input: '',
      });
    }
  };

  let msgIndex = 0;
  return (
    <div className="Support">
      <div className="Support-chatbox">
        {data.messages.map((msg) => {
          msgIndex++;
          return (
            <div key={msgIndex}>
              <p>{msg.author}</p>
              <p>{msg.message}</p>
            </div>
          );
        })}
      </div>
      <input
        className="Support-Textarea"
        placeholder="Type your messsage here..."
        onChange={updateInput}
        value={data.input}
      />
      <p>
        <button onClick={handleMessage}>Send Message</button>
      </p>
    </div>
  );
};

export default Support;
