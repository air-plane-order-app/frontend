import React, { useEffect, useState, useContext } from 'react';
import './Booking.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useHistory, useLocation } from 'react-router-dom';
import { SocketContext } from '../../../context/socket/Socket';
import { useSelector } from 'react-redux';

const seatData = {
  seat: [
    'A1',
    'A2',
    'A3',
    'B1',
    'B2',
    'B3',
    'C1',
    'C2',
    'C3',
    'D1',
    'D2',
    'D3',
    'E1',
    'E2',
    'E3',
    'F1',
    'F2',
    'F3',
    'G1',
    'G2',
    'G3',
    'H1',
    'H2',
    'H3',
  ],
  seatAvailable: [
    'A1',
    'A2',
    'A3',
    'B1',
    'B2',
    'B3',
    'C1',
    'C2',
    'C3',
    'D1',
    'D2',
    'D3',
    'E1',
    'E2',
    'E3',
    'F1',
    'F2',
    'F3',
    'G1',
    'G2',
    'G3',
    'H1',
    'H2',
    'H3',
  ],
  seatReserved: [],
}

const roomId = btoa(Math.random().toString() + new Date().getTime().toString());

export const Booking = () => {
  const socket = useContext(SocketContext);
  const user = useSelector((state) => state.auth.user);

  const [data, setData] = useState(seatData);
  const [isSharing, setIsSharing] = useState(false)
  const [bookingMode, setBookingMode] = useState(false)
  const [shareCode, setShareCode] = useState(null)
  const [roomCode, setRoomCode] = useState(null)
  const [messageList, setMessageList] = useState([]);
  
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    if (!!location.state?.shareCode) {
      setIsSharing(true);
      setRoomCode(location.state?.shareCode);
      setBookingMode(true)
      
      socket.emit('joinBooking', { name: user?.name, room: location.state?.shareCode,  isRoot: false });
    }
  },[location])

  useEffect(() => {
    socket.on('booking', ({ author, message, seat }) => {
      console.log(`${author}: `, message);

      let messages = [...messageList];

      messages.push({ author, message });

      setMessageList(messages);

      console.log(seat)
      if (!!seat) {
        if (data.seatReserved.indexOf(seat) > -1) {
          setData({
            ...data,
            seatAvailable: data?.seatAvailable.concat(seat),
            seatReserved: data?.seatReserved.filter((res) => res != seat),
          });
        } else {
          setData({
            ...data,
            seatReserved: data?.seatReserved.concat(seat),
            seatAvailable: data?.seatAvailable.filter((res) => res != seat),
          });
        }
      }
    });
  });

  const onClickData = (seat) => {
    if (!bookingMode) {
      if (data.seatReserved.indexOf(seat) > -1) {
        setData({
          ...data,
          seatAvailable: data?.seatAvailable.concat(seat),
          seatReserved: data?.seatReserved.filter((res) => res != seat),
        });
      } else {
        setData({
          ...data,
          seatReserved: data?.seatReserved.concat(seat),
          seatAvailable: data?.seatAvailable.filter((res) => res != seat),
        });
      }
    }

    socket.emit('booking', { name: user?.name, seat: seat, room: roomCode ?? roomId})
  };

  const generateShareCode = () => {
    socket.emit('joinBooking', { name: user?.name, room: roomId, isRoot: true });

    setShareCode(roomId)
    setBookingMode(true)

    setData(seatData)
  }

  const onContinue = () => {
    history.replace({
      pathname: '/confirm',
      state: {
        ...location?.state,
        seat: data?.seatReserved.toString(),
      },
    });
  };

  return (
    <div className="Seat-Container">
      <h1>Seat Reservation System</h1>
      <div className="container">
        <div className="first-container">
          <div className="seat-row">
            <table className="grid">
              <tbody>
                <tr>
                  {data?.seat.map((row) => (
                    <td
                      className={
                        data?.seatReserved.indexOf(row) > -1
                          ? 'reserved'
                          : 'available'
                      }
                      key={row}
                      onClick={(e) => onClickData(row)}
                    >
                      {row}{' '}
                    </td>
                  ))}
                </tr>
              </tbody>
            </table>
          </div>

          <div className="container-row">
            <div className="left">
              <h4>
                Available Seats: (
                {data?.seatAvailable?.length == 0
                  ? 'No seats available'
                  : data?.seatAvailable?.length}
                )
              </h4>
              <ul>
                {data?.seatAvailable.map((res) => (
                  <li key={res}>{res}</li>
                ))}
              </ul>
            </div>

            <div className="right">
              <h4>Reserved Seats: ({data?.seatReserved.length})</h4>
              <ul>
                {data?.seatReserved.map((res) => (
                  <li key={res}>{res}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>

        {!isSharing && (<div>
          <div className="container-invite">
            <button onClick={generateShareCode} className="btn btn-secondary">Invite Your Friend</button>

            <input value={shareCode} class="form-control" type="text" readonly />
          </div>

          <div className="container-button">
            <button className="btn btn-primary" onClick={onContinue}>
              Continue
            </button>
          </div>
        </div>)}

        <div className="notify-container">
          {messageList.map(item => {
            return (
              <div className="notify" key={item}>
                <h6>{item?.message}</h6>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  );
};
