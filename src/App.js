import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Cookies from 'universal-cookie';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './app/components/childs/navbar/Navbar'
import './App.css'

import { isLoggedIn, updateUser } from './store/authReducer';
import { getUser, getToken } from './app/core/apis';

import WebRouter from './app/components/WebRouter';
import CmsRouter from './app/features/CMS/CmsRouter';

const App = () => {

  const dispatch = useDispatch();
  const cookies = new Cookies();

  //Checking if user already authenticated
  useEffect(() => { (async () => {
      const cookieData = cookies.get('isLoggedIn');
      if (cookieData === 'true') {
        const token = cookies.get('token');
        // console.log('tokentoken',token);
        const data = { result: true, token: token };
        dispatch(isLoggedIn(data));

        const user = await getUser();
        // console.log('Current USer',user)
        if (user?.data) {
          dispatch(updateUser({user: user.data, isAdmin: user.data?.role?.name}));
        }
      }
    })();
  }, []);

  return (
    <div>
      {/* Web */}
      <Router>
        <div className="App">
          <img className="background" src="https://i.ibb.co/HqzDKwm/background.jpg"/>
          <Navbar></Navbar>
          <WebRouter />
        </div>
        <Route path="/cms" component={CmsRouter}/>
      </Router>
    </div>
  );
};

export default App;
